import { CSSResultGroup, html } from "lit";
import BoldElement from "../bold-element";
import { customElement, property, query, state } from "lit/decorators.js";
import styles from "./reaction-picker.styles";
import reactions from "./reactions.json";
import { classMap } from "lit/directives/class-map.js";
import { ifDefined } from "lit/directives/if-defined.js";

/**
 * @summary Reaction picker allows the user to react using emoji
 *
 * @fires emoji-selected - Indicates when the emoji selected.
 *
 * @csspart container - The container that wraps reaction picker element.
 * @csspart popover - The content box triggered by a button.
 * @csspart trigger - The button that triggers the popover.
 * @csspart helper-text - A text providing additional context.
 */
@customElement("bold-reaction-picker")
export class BoldReactionPicker extends BoldElement {
  static styles: CSSResultGroup = [super.styles, styles];

  @query(".reactionpicker__react") pickerButton!: HTMLButtonElement;
  @query(".reactionpicker__popover") pickerPopover!: HTMLDivElement;
  @query(".reactionpicker__emoji") flyingEmoji!: HTMLSpanElement;

  @property({ type: String, attribute: "helpertext" }) helperText: string = "";
  @property({ reflect: true }) variant:
    | "basiclike"
    | "lovemeh"
    | "moviegoer"
    | "tothemoon"
    | "rainbowlove" = "basiclike";

  @state() disabled: boolean = false;
  @state() open: boolean = false;
  @state() selected: string | undefined;
  @state() lastEmoji: string | undefined;

  get value(): string[] {
    return reactions[this.variant] || [];
  }

  firstUpdated() {
    // Sets popover position which depends on the number of emojis
    this.setPopOverPosition();

    // Adds flying emoji handler when animation is started and ended
    this.flyingEmoji.addEventListener(
      "animationstart",
      this.flyingEmojiStartHandles
    );
    this.flyingEmoji.addEventListener(
      "animationend",
      this.flyingEmojiEndHandles
    );
  }

  flyingEmojiStartHandles = () => {
    // Disables the picker button when the animation starts
    this.disabled = true;
  };

  flyingEmojiEndHandles = () => {
    // Resets the "selected" and "disabled" state when the animation starts
    this.selected = undefined;
    this.disabled = false;
  };

  connectedCallback() {
    super.connectedCallback();
    document.addEventListener("click", this.closePopoverHandler);
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    document.removeEventListener("click", this.closePopoverHandler);
  }

  // Closes the popover whenever a click event occurs anywhere on the document (but the picker)
  closePopoverHandler = (e: MouseEvent) => {
    if (e.target !== this) {
      this.open = false;
    }
  };

  // Adjusts the popover position when it's open
  setPopOverPosition = () => {
    if (this.value.length <= 6) {
      this.pickerPopover.style.gridTemplateColumns = `repeat(${this.value.length}, 1fr)`;
    } else {
      this.pickerPopover.style.gridTemplateColumns = `repeat(${Math.round(
        this.value.length / 3
      )}, 1fr)`;
    }
    this.pickerPopover.style.top = `${-this.pickerPopover.clientHeight - 8}px`;
  };

  pickerButtonHandler = () => {
    this.open = !this.open;
  };

  emojiSelectedHandler = (_e: MouseEvent, emoji: string) => {
    this.open = false;
    this.selected = emoji;
    this.lastEmoji = emoji;

    this.dispatchEvent(
      new CustomEvent("emoji-selected", {
        detail: this.selected,
      })
    );
  };

  render() {
    return html`<div class="reactionpicker" part="container">
      <div
        class=${classMap({
          reactionpicker__popover: true,
          "reactionpicker--open": this.open,
        })}
        part="popover"
      >
        ${this.value.map(
          (emoji) =>
            html`<button
              class="reactionpicker__option reactionpicker__button reactionpicker__button--flex"
              @click="${(e: MouseEvent) => this.emojiSelectedHandler(e, emoji)}"
            >
              ${emoji}
            </button>`
        )}
      </div>
      <span
        class=${classMap({
          reactionpicker__emoji: true,
          "reactionpicker__emoji--flying": this.selected ? true : false,
        })}
        >${this.selected}</span
      >
      <button
        class=${classMap({
          reactionpicker__react: true,
          reactionpicker__button: true,
          "reactionpicker__button--flex": true,
          "reactionpicker__react--active": this.open,
          "reactionpicker__react--disabled": this.disabled,
        })}
        @click="${this.pickerButtonHandler}"
        ?disabled=${ifDefined(this.disabled)}
        part="trigger"
      >
        <span class="reactionpicker__label">${this.lastEmoji || this.value[0]}</span>
      </button>
      <span part="helper-text">${this.helperText}</span>
    </div>`;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "bold-reaction-picker": BoldReactionPicker;
  }
}
