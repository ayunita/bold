import { css } from "lit";

export default css`
  :host {
    display: inline-block;
    width: auto;
  }

  .reactionpicker {
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .reactionpicker__popover {
    padding: 8px;
    display: grid;
    gap: 0.5rem;
    border: var(--bold-border);
    border-radius: 8px;
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
    z-index: 9999;
    background-color: var(--color-white);
    visibility: hidden;
  }

  .reactionpicker--open {
    visibility: visible;
  }

  .reactionpicker__button {
    border: none;
    cursor: pointer;
    background-color: transparent;
  }

  .reactionpicker__button--flex {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .reactionpicker__option {
    width: 32px;
    height: 32px;
    border-radius: 4px;
    font-size: 1rem;
  }

  .reactionpicker__emoji {
    position: absolute;
    top: -20px;
    font-size: 1.5rem;
  }

  .reactionpicker__emoji--flying {
    animation: flying 3s forwards;
  }

  @keyframes flying {
    0% {
      opacity: 0;
    }
    25% {
      opacity: 1;
    }
    100% {
      opacity: 0;
      top: calc(100vh * -0.5);
    }
  }

  .reactionpicker__option:hover {
    border: 1px solid rgba(0, 0, 0, 0.1);
  }

  .reactionpicker__react {
    width: 42px;
    height: 42px;
    font-size: 1.2rem;
    border-radius: 8px;
    border: var(--bold-border);
    background-color: var(--color-white);
  }

  .reactionpicker__react--active {
    background-color: var(--color-black);
  }

  .reactionpicker__react--disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }
`;
