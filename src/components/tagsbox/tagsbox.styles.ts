import { css } from "lit";

export default css`
  :host {
    display: inline-block;
    position: relative;
    width: 100%;
  }

  .tagsbox {
    display: grid;
    gap: 1rem;
    border: var(--bold-border);
    padding: 16px;
    background-color: var(--color-white);
  }

  .tagsbox:focus-within {
    outline: -webkit-focus-ring-color auto thin;
  }

  .tagsbox__output {
    display: inline-flex;
    flex-wrap: wrap;
    gap: 1rem;
  }

  .tagsbox__input {
    outline: none;
    border: none;
    border-bottom: var(--bold-border);
    background-color: transparent;
    font-size: 1rem;
  }

  .tagsbox__button {
    background-color: transparent;
    border: none;
    cursor: pointer;
  }
  

  .tagsbox__helper {
    font-size: .7rem;
    color: var(--color-muted);
  }

`;
