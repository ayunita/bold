import { CSSResultGroup, html } from "lit";
import BoldElement from "../bold-element";
import { BoldBadge } from "../badge/badge";
import { customElement, property } from "lit/decorators.js";
import styles from "./tagsbox.styles";
import { repeat } from "lit/directives/repeat.js";

/**
 * @summary A text input field that displays multiple tags or keywords.
 *
 * @fires item-changed - Indicates when an item/tag is added or removed.
 * 
 * @dependency bold-badge
 * 
 * @slot - A content to be displayed when value is empty.
 * 
 * @csspart container - The container that wraps input and badges.
 * @csspart empty - The container that wraps the content when value is empty.
 * @csspart output - The container that wraps the items/tags.
 * @csspart remove-button - The remove item button.
 * @csspart input - The text input field.
 * @csspart helper-text - A text providing additional context.
 */
@customElement("bold-tagsbox")
export class BoldTagsbox extends BoldElement {
  static styles: CSSResultGroup = [super.styles, styles];

  static dependencies = {
    "bold-badge": BoldBadge,
  };

  @property() placeholder = "";
  @property({ attribute: "helpertext" }) helperText = "";
  @property({
    reflect: true,
    converter: (attrValue: string | null) => {
      if (attrValue) return attrValue.split(/,/g);
      else return undefined;
    },
  })
  value: string[] = [];

  inputKeydownHandler(e: KeyboardEvent) {
    if (e.key === "Enter") {
      const inputElement = e.target as HTMLInputElement;
      this.value?.push(inputElement.value);
      this.requestUpdate("value");
      inputElement.value = "";

      this.dispatchEvent(
        new CustomEvent("item-changed", {
          detail: this.value,
        })
      );
    }
  }

  buttonClickHandler(index: number) {
    this.value.splice(index, 1);
    this.requestUpdate("value");

    this.dispatchEvent(
      new CustomEvent("item-changed", {
        detail: this.value,
      })
    );
  }

  render() {
    return html` <div part="container" class="tagsbox">
      <div part="output" class="tagsbox__output">
        ${this.value.length > 0
          ? repeat(
              this.value,
              (str: string, index: number) => html`<bold-badge>${str}
                <button
                  class="tagsbox__button"
                  part="remove-button"
                  aria-label="Remove item ${str}"
                  tabindex="0"
                  slot="suffix"
                  @click="${() => this.buttonClickHandler(index)}"
                >
                  <span aria-hidden="true">✖</span>
                </button></bold-badge>`
            )
          : html`<slot part="empty" class="tagsbox__empty"></slot>`}
      </div>
      <input
        class="tagsbox__input"
        type="text"
        autocapitalize="off"
        autocomplete="off"
        autocorrect="off"
        placeholder="${this.placeholder}"
        spellcheck="false"
        @keydown="${this.inputKeydownHandler}"
      />
      <span part="helper-text" class="tagsbox__helper">${this.helperText}</span>
    </div>`;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "bold-tagsbox": BoldTagsbox;
  }
}
