import { beforeEach, describe, expect, it } from "vitest";

import "./badge";

describe("<bold-badge>", async () => {
  function getBadge(): HTMLElement | null | undefined {
    return document.body.querySelector("bold-badge");
  }

  describe("when provided no parameters", () => {
    beforeEach(() => {
      document.body.innerHTML = "<bold-badge>Badge</bold-badge>";
    });

    it("renders the label", async () => {
      expect(getBadge()?.textContent).toContain("Badge");
    });
  });

  describe("when provided pill parameter", () => {
    beforeEach(() => {
      document.body.innerHTML = "<bold-badge pill>Badge</bold-badge>";
    });

    it("appends badge--pill to class list", async () => {
      expect(
        getBadge()?.shadowRoot?.querySelector(".badge")?.classList.value.trim()
      ).toContain("badge badge--pill");
    });
  });
});
