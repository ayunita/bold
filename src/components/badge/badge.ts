import { CSSResultGroup, html } from "lit";
import BoldElement from "../bold-element";
import { customElement, property } from "lit/decorators.js";
import styles from "./badge.styles";
import { classMap } from "lit/directives/class-map.js";

/**
 * @summary A visual indicator used to convey status, identity, or other attributes within a user interface.
 *
 * @slot - The badge's label.
 * @slot prefix - A content to be displayed before the badge's label. 
 * @slot suffix - A content to be displayed after the badge's label.
 * 
 * @csspart prefix - The container that wraps the prefix.
 * @csspart label - The badge's label.
 * @csspart suffix - The container that wraps the suffix.
 */
@customElement("bold-badge")
export class BoldBadge extends BoldElement {
  static styles: CSSResultGroup = [super.styles, styles];

  @property({ type: Boolean, reflect: true }) pill = false;

  render() {
    return html` <span
      class=${classMap({
        badge: true,
        "badge--pill": this.pill
      })}
    >
      <slot name="prefix" part="prefix" class="badge__prefix"></slot>
      <slot part="label" class="badge__label"></slot>
      <slot name="suffix" part="suffix" class="badge__suffix"></slot>
    </span>`;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "bold-badge": BoldBadge;
  }
}
