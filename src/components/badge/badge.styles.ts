import { css } from "lit";

export default css`
  :host {
    display: inline-block;
    position: relative;
    width: auto;
  }

  .badge {
    display: inline-flex;
    justify-content: center;
    align-items: center;
    gap: 0.3rem;
    border: var(--bold-border);
    padding: .2rem .5rem;
    background-color: var(--color-white);
  }

  .badge--pill {
    border-radius: 1rem;
  }

  .button__label {
    display: inline-block;
  }

  .badge__prefix,
  .button__suffix {
    flex: 0 0 auto;
  }
`;
