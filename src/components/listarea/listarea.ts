import { CSSResultGroup, html } from "lit";
import DOMPurify from "isomorphic-dompurify";
import BoldElement from "../bold-element";
import { customElement, property, query } from "lit/decorators.js";
import styles from "./listarea.styles";

/**
 * @summary A textarea-like interface that displays the content as a bullet list.
 *
 * @fires input-changed - Indicates when the input changes.
 *
 * @csspart container - The container that wraps contenteditable element.
 * @csspart editable - The contenteditable element for text input.
 */
@customElement("bold-listarea")
export class BoldListarea extends BoldElement {
  static styles: CSSResultGroup = [super.styles, styles];

  @query(".listarea__editable") editable!: HTMLDivElement;
  @property({ reflect: true }) value: string = "[]";

  firstUpdated() {
    this.updateEditableContent();
  }

  updateEditableContent() {
    const array = JSON.parse(this.value.replace(/'/g, '"'));
    this.editable.innerHTML = array
      .map((str: string) => `<div>${str}</div>`)
      .join("");
  }

  sanitizeHTML(htmlString: string) {
    return DOMPurify.sanitize(htmlString, {
      ALLOWED_TAGS: [],
      ALLOWED_ATTR: [],
    });
  }

  inputHandler() {
    const parsedValue = this.editable.innerText
      .split(/\r?\n/)
      .map((str: string) => this.sanitizeHTML(str))
      .filter((str) => str.trim() !== "");

    this.value = JSON.stringify(parsedValue);

    this.dispatchEvent(
      new CustomEvent("input-changed", {
        detail: parsedValue,
      })
    );
  }

  render() {
    return html` <div part="container" class="listarea">
      <div
        part="editable"
        class="listarea__editable"
        role="textbox"
        aria-multiline="true"
        tabindex="0"
        contenteditable="true"
        @input="${this.inputHandler}"
      ></div>
    </div>`;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "bold-listarea": BoldListarea;
  }
}
