import { css } from "lit";

export default css`
  :host {
    display: inline-block;
    position: relative;
    width: 100%;
  }

  .listarea {
    border: var(--bold-border);
    padding: 8px;
    background-color: var(--color-white);
  }

  .listarea:focus-within {
    outline: -webkit-focus-ring-color auto thin;
  }

  .listarea__editable {
    margin-left: 16px;
    outline: none;
    display: list-item;
    word-wrap: break-word;
    background-color: transparent;
  }

  .listarea__editable > div {
    display: list-item;
  }
`;
