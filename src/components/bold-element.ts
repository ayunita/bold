import { css, CSSResultGroup, LitElement } from "lit";

export default class BoldElement extends LitElement {
  static styles: CSSResultGroup = css`
    :host {
      display: inline-block;

      --color-black: #0c0c0c;
      --color-white: #ffffff;
      --color-muted: #636c72;

      --bold-border: 2px solid var(--color-black);
    }
  `;
}
