import { CSSResultGroup, html } from "lit";
import BoldElement from "../bold-element";
import { customElement, property, state } from "lit/decorators.js";
import styles from "./input-cc.styles";
import { classMap } from "lit/directives/class-map.js";
import CardValidator from "card-validator";

/**
 * @summary A custom element for entering credit card information.
 *
 * @fires input-changed - Indicates when the input on credit card information changes.
 *
 * @csspart container - The container that wraps input elements for credit card.
 * @csspart container-holder - The card holder field's wrapper.
 * @csspart container-number - The card number field and type label's wrapper.
 * @csspart container-expiry - The card expiry date field's wrapper.
 * @csspart container-cvv - The card cvv field's wrapper.
 */
@customElement("bold-inputcc")
export class BoldInputCC extends BoldElement {
  static styles: CSSResultGroup = [super.styles, styles];

  @property({ type: Boolean, reflect: true }) pill = false;
  @state() holder: string = "";
  @state() number: string = "";
  @state() expiry: string = "";
  @state() cvv: string = "";
  @state() type: string | undefined;
  @state() valid: {
    number: boolean | undefined;
    expiry: boolean | undefined;
    cvv: boolean | undefined;
  } = {
    number: undefined,
    expiry: undefined,
    cvv: undefined,
  };

  holderInputHandler(e: MouseEvent) {
    const inputElement = e.target as HTMLInputElement;
    this.holder = inputElement.value;
    this.inputCCChangeHandler();
  }

  numberInputHandler(e: MouseEvent) {
    const inputElement = e.target as HTMLInputElement;
    this.number = inputElement.value;
    const validator = CardValidator.number(this.number);
    this.valid.number = validator.isValid;
    this.type = this.valid.number ? validator.card?.type : undefined;
    this.inputCCChangeHandler();
  }

  cvvInputHandler(e: MouseEvent) {
    const inputElement = e.target as HTMLInputElement;
    this.cvv = inputElement.value;
    const validator = CardValidator.cvv(this.cvv);
    this.valid.cvv = validator.isValid;
    this.inputCCChangeHandler();
  }

  expiryInputHandler(e: MouseEvent) {
    const inputElement = e.target as HTMLInputElement;
    this.expiry = inputElement.value;
    const validator = CardValidator.expirationDate(this.expiry);
    this.valid.expiry = validator.isValid;
    this.inputCCChangeHandler();
  }

  inputCCChangeHandler() {
    this.dispatchEvent(
      new CustomEvent("input-changed", {
        detail: {
          holder: this.holder,
          type: this.valid.number ? this.type : "invalid",
          number: this.valid.number ? this.number : "invalid",
          expiry: this.valid.expiry ? this.expiry : "invalid",
          cvv: this.valid.cvv ? this.cvv : "invalid",
        },
      })
    );
  }

  render() {
    return html` <div
      class=${classMap({
        inputcc: true,
        "inputcc--pill": this.pill,
      })}
      part="container"
    >
      <div class="inputcc__grid inputcc__holder" part="container-holder">
        <label for="card-holder" class="inputcc__label">Card holder</label>
        <input
          id="card-holder"
          name="card-holder"
          type="text"
          placeholder="e.g. Anna Smith"
          .value=${this.holder}
          @input="${this.holderInputHandler}"
        />
      </div>
      <div class="inputcc__grid inputcc__number" part="container-number">
        <label for="card-number" class="inputcc__label">Card number</label>
        <div>
          <input
            id="card-number"
            name="card-number"
            type="text"
            placeholder="xxxx xxxx xxxx xxxx"
            .value=${this.number}
            @input="${this.numberInputHandler}"
          />
          <span class="inputcc__type">${this.type}</span>
        </div>
      </div>
      <div class="inputcc__grid inputcc__expiry" part="container-expiry">
        <label for="card-expiry" class="inputcc__label">Expiration</label>
        <input
          id="card-expiry"
          name="card-expiry"
          type="text"
          placeholder="MM/YY"
          .value=${this.expiry}
          @input="${this.expiryInputHandler}"
        />
      </div>
      <div class="inputcc__grid inputcc__cvv" part="container-cvv">
        <label for="card-cvv" class="inputcc__label">CVV</label>
        <input
          id="card-cvv"
          name="card-cvv"
          type="text"
          placeholder="xxx"
          .value=${this.cvv}
          @input="${this.cvvInputHandler}"
        />
      </div>
    </div>`;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "bold-inputcc": BoldInputCC;
  }
}
