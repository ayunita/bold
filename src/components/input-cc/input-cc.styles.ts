import { css } from "lit";

export default css`
  :host {
    display: inline-block;
    position: relative;
    width: auto;
  }

  .inputcc {
    border: var(--bold-border);
    padding: 1rem;
    background-color: var(--color-white);
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    gap: .5rem;
  }

  .inputcc input {
    width: 100%;
    border: none;
    border-bottom: var(--bold-border);
    background-color: transparent;
    font-size: 1rem;
    padding: .5rem 0;  
  }

  .inputcc__label {
    text-transform: uppercase;
    letter-spacing: .1rem;
  }

  .inputcc__grid {
    display: grid;
    gap: .5rem;
  }

  .inputcc__holder, .inputcc__number {
    grid-column: span 2;
  }

  .inputcc__number {
    position: relative;
  }

  .inputcc__type {
    color: var(--color-muted);
    font-size: .7rem;
    position: absolute;
    right: 0;
    top: 0;
  }

  .inputcc--pill {
    border-radius: 1rem;
  }
`;
