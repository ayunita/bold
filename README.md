<div align='center'>
    <h1><b>Bold</b></h1>
    <img src='https://gitlab.com/uploads/-/system/project/avatar/56513501/logo.png?width=150' width='150' height='150' />
    <p>A library of random, simple, BOLD web components</p>

</div>

---

## ⭐ **ABOUT**

The **Bold** Web Component Library is a collection of reusable UI components built using Lit.

Feel free to use and change it!
<br />

---

## 🛠️ **INSTALLATION**

### local installation:

1. clone the repo

```
git clone https://gitlab.com/ayunita/bold.git
```

2. cd into cloned repo

```
cd bold
```

3. install dependencies

```
yarn
```

4. run the app

```
yarn dev
```

<br />

## 🔎 **SHOWCASE**

[Demo page](https://bold-ayunita-3740cc642c7ca2fc97b7de0a90e40ad432df660778ba7fc607.gitlab.io)
<br />

---

## 💻 **TECHNOLOGIES**
![Lit](https://img.shields.io/badge/lit-324FFF?style=for-the-badge&logo=lit&logoColor=white)
![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
![Vite](https://img.shields.io/badge/vite-%23646CFF.svg?style=for-the-badge&logo=vite&logoColor=white)

<br />